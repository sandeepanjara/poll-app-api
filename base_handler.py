import json
import traceback
from datetime import datetime
from pprint import pprint

from tornado.gen import coroutine
from tornado.web import RequestHandler


class BaseHandler(RequestHandler):

    def initialize(self):

        """It will set the given response headers"""

        self.set_header('Content-Type', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "false")
        self.set_header("Access-Control-Expose-Headers", "*")
        self.set_header("Access-Control-Allow-Methods", "*")
        self.set_header("Access-Control-Allow-Headers", "*")
        self.body = dict()
        self._parsed = False
        self.result = dict()
        self.db = self.settings["client"].poll
        self._response = None

    def write_error(self, status_code, **kwargs):
        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            # in debug mode, try to send a traceback
            self.set_header('Content-Type', 'text/plain')
            for line in traceback.format_exception(*kwargs["exc_info"]):
                self.write(line)
            self.finish()
        else:
            self.set_header('Content-Type', '*')
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Access-Control-Allow-Methods", "*")
            self.set_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
            self.set_header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
            self._status_code = 200
            data = json.dumps(dict(
                code=status_code,
                message=kwargs.get("exc_info").__str__()
            ))

    def set_result(self, result):
        self.result = result

    def options(self, *args, **kwargs):
        self.send_error(200)

    def parse_request_body(self):
        try:
            if self._parsed is False:
                self.body = json.loads(self.request.body.decode())
                self._parsed = True
            return self.body
        except Exception as e:
            self.respond(str(e), 500)

    def get_json_body_argument(self, key, default=None):
        try:
            if self._parsed is False:
                self.body = self.parse_request_body()
            return self.body[key]
        except KeyError:
            if default is not None:
                return default
            else:
                raise Exception("The key " + key + " is missing in request body")

    def respond(self, response, status_code):
        try:
            self._response = {"message": response, "status": status_code}
            data = json.dumps(self._response)
            self.write(data)
        except Exception as err:
            self.write(err.__str__())
