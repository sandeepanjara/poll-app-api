from base_handler import BaseHandler
from tornado.gen import coroutine


class LoginHandler(BaseHandler):
    @coroutine
    def post(self, *args, **kwargs):
        id = self.get_json_body_argument("id")
        password = self.get_json_body_argument("password")
        db = self.settings['client'].poll
        result = yield db.user.find_one({"id": id})
        if result:
            if result["password"] == password:
                self.respond({"token": result["_id"].__str__(), "name": result["id"]}, 200)
            else:
                self.respond("Invalid password", 401)
        else:
            self.respond("Invalid user name", 403)
