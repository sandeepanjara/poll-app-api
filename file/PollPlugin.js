var count;
var obj;
$(document).ready(function () {


    $.ajax({
            url: "http://13.126.201.174:8000" + '/user/question?user=' + "here",
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function (result) {
                if (result.status === 200) {
                    obj = result.message;
                    var opts = obj.options;
                    var temp = '';
                    temp += '<div id="divOptions">';
                    temp += '<form> Question: ' + obj.question + '<br/>';
                    count = 0;
                    for (var i = 0; i < opts.length; i++) {
                        temp += '<input name="r1" type="radio" id="op' + i + '" value="">   ' + opts[i] + ' <br/>';
                    }
                    temp += '<button type="button" onclick="sendAnswer(obj)">Send Answer</button><br/></form> </div>';
                    temp += '<div id="resOptions" style="display: none"> </div>';


                    $('body').append('<div id="myModal" class="modal fade" role="dialog">' +
                        '<div class="modal-dialog">' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                        '<h4 class="modal-title">Poll</h4>' +
                        '</div>' +
                        '<div class="modal-body">' + temp + '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                    $("#myModal").modal({
                        backdrop: false
                    });
                } else {

                }
            },
            error: function () {
            }
        }
    );


});

function sendAnswer(obj) {
    var count;
    var i = 0;
    for (; i < obj.options.length; i++) {
        if (document.getElementById("op" + i).checked) {
            count = i;
        }
    }

    obj.answer[count]=parseInt(obj.answer[count])+1;
    $.ajax({
            url: config.baseUrl + '/answer',
            type: 'POST',
            crossDomain: true,
            async: false,
            dataType: 'json',
            data: JSON.stringify({"qid": obj.qid, "answer": count}),
            success: function (result) {
                if (result.status === 200) {
                    var resStr = '';
                    var k = 0;
                    resStr += "<table style='padding: 20px'>";
                    for (; k < obj.answer.length; k++) {
                        resStr += "<tr><td> Option:&nbsp;"  + obj.options[k] + "</td><td> &nbsp;&nbsp;  Votes:&nbsp; " + obj.answer[k] + "</td></tr>";

                    }
                    resStr += "</table>";
                    document.getElementById("divOptions").style.display = "none";
                    document.getElementById("resOptions").innerHTML = resStr;
                    document.getElementById("resOptions").removeAttribute("style");
                } else {

                }
            },
            error: function () {
            }
        }
    );

}
