from motor import MotorClient
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.web import Application, StaticFileHandler
from base_handler import BaseHandler
from pymongo import MongoClient
from login import LoginHandler
from question import QuestionHandler
from answer import AnswerHandler
from user_question import UserQuestionHandler
from signup import SignUpHandler
define(name="port", default=8000, help="run on the given port", type=int)

client = MotorClient()


class Handle(BaseHandler):
    def get(self, *args, **kwargs):
        self.respond('OK', 200)


def get_app():
    return Application(handlers=[
        (r"/", Handle),
        (r"/login", LoginHandler),
        (r"/question", QuestionHandler),
        (r"/answer", AnswerHandler),
        (r"/user/question", UserQuestionHandler),
        (r"/signup", SignUpHandler),
        (r'/file/(.*)', StaticFileHandler, {'path': r"C:\Users\Sandeep\PycharmProjects\Poll\file"})
    ],
        client=client,
        static_path=r"C:\Users\Sandeep\PycharmProjects\Poll\file"
    )

port = options.port


def get_http_server(application):
    return HTTPServer(application)

if __name__ == '__main__':

    options.parse_command_line()
    app = get_app()
    http_server = get_http_server(app)
    http_server.listen(options.port)
    IOLoop.current().start()
