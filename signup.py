
from base_handler import BaseHandler
from file.file_creator import file_creator
from tornado.gen import coroutine


class SignUpHandler(BaseHandler):
    @coroutine
    def post(self, *args, **kwargs):
        db = self.settings["client"].poll
        data = self.parse_request_body()
        result = yield db.user.find_one({"id":data["id"]})
        if result:
            self.respond("Already registered", 200)
        else:
            result = yield db.user.insert(data)
            if result:
                file_creator(data["id"])
                self.respond("Successfully registered", 200)
            else:
                self.respond("Please try again", 200)
