from base_handler import BaseHandler
from bson import ObjectId
from authenticate import authenticate
from tornado.gen import coroutine


class QuestionHandler(BaseHandler):
    @authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.parse_request_body()
        db = self.settings["client"].poll
        data["answer"] = [0 for i in data["options"]]
        data["open"] = True
        yield db.question.update({"user": data["user"], "open": True}, {"$set": {"open": False}})
        result = yield db.question.insert(data)
        if result:
            self.respond("Success", 200)
        else:
            self.respond("Error", 200)

    @authenticate
    @coroutine
    def get(self, *args, **kwargs):
        db = self.settings["client"].poll
        user = self.get_argument("user")
        questions = []
        result = db.question.find({"user": user}).sort("_id", -1)
        while(yield result.fetch_next):
            question = result.next_object()
            question["qid"] = question["_id"].__str__()
            if question["open"]:
                question["status"] = "open"
            else:
                question["status"] = "closed"
            del question["_id"]
            del question["open"]
            questions.append(question)
        self.respond(questions, 200)

    @authenticate
    @coroutine
    def put(self, *args, **kwargs):
        db = self.settings['client'].poll
        qid = ObjectId(self.get_json_body_argument("qid"))
        user = self.get_json_body_argument("user")
        yield db.question.update({"user": user, "open": True}, {"$set":{"open": False}})
        yield db.question.update({"_id": qid}, {"$set": {"open": True}})
        self.respond("Success", 200)

    @authenticate
    @coroutine
    def delete(self, *args, **kwargs):
        db = self.settings['client'].poll
        qid = ObjectId(self.get_json_body_argument("qid"))
        yield db.question.remove({"_id": qid})
        self.respond("Success", 200)
