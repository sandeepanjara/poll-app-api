from base_handler import BaseHandler
from bson import ObjectId
from tornado.gen import coroutine


class AnswerHandler(BaseHandler):
    @coroutine
    def post(self, *args, **kwargs):
        qid = ObjectId(self.get_json_body_argument("qid"))
        db = self.settings["client"].poll
        answer_id = self.get_json_body_argument("answer")
        index = "answer." + answer_id.__str__()
        updated = yield self.db.question.update({"_id": qid}, {"$inc": {index: 1}})
        if updated:
            self.respond("Success", 200)
        else:
            self.respond("Error", 200)



