
from base_handler import BaseHandler
from tornado.gen import coroutine


class UserQuestionHandler(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        db = self.settings["client"].poll
        user = self.get_argument("user")
        result = yield db.question.find_one({"user": user, "open": True})
        result["qid"] = result["_id"].__str__()
        del result["_id"]
        self.respond(result, 200)
