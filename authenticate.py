
from functools import wraps

from bson import ObjectId
from tornado.gen import coroutine


def authenticate(function):
    @wraps(function)
    @coroutine
    def wrapper(self, *args, **kwargs):
        db = self.settings["client"].poll
        token = self.request.headers.get("Authorization")
        if token == "null":
            self.respond("Forbidden", 403)
        if token is None:
            self.respond("Forbidden", 403)
        else:
                print(token)
                token = ObjectId(token)
                result = yield db.user.find_one({"_id": token}, {"token": 1})
                if result is not None:
                    yield function(self, *args, **kwargs)
                else:
                    self.respond("Invalid Token", 401)
    return wrapper
